# Prerequisites
```
docker network create east1
docker network create east2
```
# Start example

- start configuration ```cd config; docker-compose up -d```

- start eureka ```cd eureka; docker-compose up -d```

- start zuul ```cd zuul; docker-compose up -d```
